"use strict"

// I don't know if this is allowed, but eh. I can't find an api-type thing.

const request = require('request')
const cheerio = require('cheerio')
const q = require('bluebird')

function getDevicePageHTML(device) {
  let url = `https://download.dirtyunicorns.com/?device=${device}`

  return new q.Promise((res, rej) => request(url, (err, resp) => {
    if (err)
      return rej(err)

    if (resp.statusCode != 200)
      return rej(resp.statusCode)

    res(resp.body)
  }))
}

function getDeviceSection(section, device) {
  return getDevicePageHTML(device).then(html => {
    let $ = cheerio.load(html)
    return $('body').find(`section.mdl-layout__tab-panel#${section}`).html()
  })
}

function parseCards(html) {
  let $ = cheerio.load(html)
  
  let cards = $('body').find('.mdl-card')
  let list = Array.from(cards)

  return q.map(list, card => ({
    file: $(card).find('.mdl-card__title-text').text(),
    link: $(card).find('.mdl-button').attr('href')
  }))
}


module.exports.getWeeklies = function(device) {
  return getDeviceSection('Weeklies', device).then(parseCards)
}

module.exports.getVendorImages = function(device) {
  return getDeviceSection('vendor_images', device).then(parseCards)
}

module.exports.getReleaseCandidates = function(device) {
  return getDeviceSection('Rc', device).then(parseCards)
}

module.exports.getOfficials = function(device) {
  return getDeviceSection('Official', device).then(parseCards)
}